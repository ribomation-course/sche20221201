package meaningful_names;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Second {
    static final int GAMEBOARD_SIZE = 32;
    static final int STATUS         = 0;
    static final int FLAGGED        = 4;
    int[][]          gameBoard;

    List<int[]> getFlaggedCells() {
        List<int[]> flaggedCells = new ArrayList<>();
        for (int[] x : gameBoard)
            if (x[STATUS] == FLAGGED)
                flaggedCells.add(x);
        return flaggedCells;
    }

    void run() {
        List<int[]> flagged = getFlaggedCells();
        System.out.println("flagged = \n" + toString(flagged));
    }

    String toString(List<int[]> cells) {
        String text = "";
        for (int[] cell : cells)
            text += Arrays.toString(cell) + "\n";
        return text;
    }

    void init() {
        Random r = new Random();
        gameBoard = new int[GAMEBOARD_SIZE][];
        for (int k = STATUS; k < gameBoard.length; k++) {
            gameBoard[k] = new int[FLAGGED];
            gameBoard[k][STATUS] = r.nextInt(6);
        }
    }

    public static void main(String[] args) {
        Second app = new Second();
        app.init();
        app.run();
    }
}
