package meaningful_names;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.*;

public class Third {
    static final int GAMEBOARD_SIZE = 32;
    List<Cell> gameBoard;

    class Cell {
        int     id;
        boolean flagged;

        public Cell(int id, boolean flagged) {
            this.id = id;
            this.flagged = flagged;
        }

        public boolean isFlagged() {
            return flagged;
        }

        public void setFlagged(boolean flagged) {
            this.flagged = flagged;
        }

        @Override
        public String toString() {
            return String.format("Cell-%d%s", id, flagged ? "-F" : "");
        }
    }

    List<Cell> getFlagged() {
        List<Cell> flagged = new ArrayList<>();
        for (Cell cell : gameBoard)
            if (cell.isFlagged()) flagged.add(cell);
        return flagged;
    }

    List<Cell> getFlagged2() {
        return gameBoard.stream()
                .filter(Cell::isFlagged)
                .collect(Collectors.toList());
    }

    void run() {
        System.out.println("flagged = " + getFlagged());
    }

    void run2() {
        System.out.println("flagged = " + getFlagged2());
    }

    void init() {
        Random r = new Random();
        gameBoard = new ArrayList<>();
        for (int k = 0; k < GAMEBOARD_SIZE; ++k) {
            gameBoard.add(new Cell(k + 1, r.nextInt(100) < 10));
        }
    }

    public static void main(String[] args) {
        Third app = new Third();
        app.init();
        app.run();
        app.run2();
    }
}
