package refactoring;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CrimesList3 {
    private static final String baseUrl          = "http://brottsplatskartan.se/api.php";
    public static final  int    MINUTES_PER_HOUR = 60;

    public static void main(String[] args) throws Exception {
        CrimesList3 app = new CrimesList3();
        app.run(args);
    }

    private void run(String[] args) throws Exception {
        int    lastHours = args.length > 0 ? Integer.parseInt(args[0]) : 72;
        String crimeType = args.length > 1 ? args[1] : "trafikolycka";
        findCrimes(crimeType, lastHours);
    }

    private void findCrimes(String crimeType, int lastHours) throws Exception {
        NodeList    events = getCrimeEvents(lastHours);
        List<Crime> crimes = getCrimes(events, crimeType);
        printCrimes(crimes, crimeType, lastHours);
    }

    private NodeList getCrimeEvents(int lastHours) throws SAXException, IOException, ParserConfigurationException {
        String   url = String.format("%s?action=getEvents&period=%d", baseUrl, lastHours * MINUTES_PER_HOUR);
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(url);
        doc.getDocumentElement().normalize();
        return doc.getElementsByTagName("event");
    }

    private List<Crime> getCrimes(NodeList events, String crimeType) throws Exception {
        int[] idx = {0};
        return Stream.generate(() -> events.item(idx[0]++))
                .limit(events.getLength())
                .map(Crime::new)
                .filter(c -> c.contains(crimeType))
                .collect(Collectors.toList())
                ;
    }

    private void printCrimes(List<Crime> crimes, String crimeType, int lastHours) {
        System.out.printf("Det har inträffat %d händelser av typ '%s' de senaste %d timmarna.%n",
                crimes.size(), crimeType, lastHours);
        crimes.stream().forEach(crime ->
                System.out.printf("%1$te %1$tb %1$tY, %1$tT - %2$s (%3$s)%n",
                        crime.date, crime.title, crime.place)
        );
    }
}
