package com.ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DemoStreams {
    public static void main(String[] args) throws IOException {
        DemoStreams app = new DemoStreams();
        app.run();
    }

    private void run() throws IOException {
        long cnt = Files.newBufferedReader(Paths.get("src/main/java/com/ribomation/DemoStreams.java"))
                .lines()
                .map(this::len)
//                .map(x -> new Crime(x))
//                .map(Crime::new)
                .count();
        System.out.printf("length: %d%n", cnt);
    }

    int len(String s) {
        return s.length();
    }

}
