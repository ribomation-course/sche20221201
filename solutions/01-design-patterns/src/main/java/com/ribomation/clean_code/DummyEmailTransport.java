package com.ribomation.clean_code;

import java.util.stream.Stream;

/**
 * Fake transport service
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 */
public class DummyEmailTransport implements EmailTransport {
    private final String smtpServer;
    private final int smtpPort;

    public DummyEmailTransport(String smtpServer, int smtpPort) {
        this.smtpServer = smtpServer;
        this.smtpPort = smtpPort;
    }

    @Override
    public void send(Message message) {
        System.out.printf("*** Pretending to send email...%n");
        System.out.printf("*** [SMTP] %s:%d%n", smtpServer, smtpPort);

        Stream.of("from", "to", "cc", "subject", "body")
                .map(key -> String.format("%-7s: %s", key, get(key, message)))
                .forEach(System.out::println);
        ;
    }

    private String get(String key, Message m) {
        try {
            return (String) m.getClass()
                    .getDeclaredMethod("get" + toTitleCase(key))
                    .invoke(m);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String toTitleCase(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }
}
