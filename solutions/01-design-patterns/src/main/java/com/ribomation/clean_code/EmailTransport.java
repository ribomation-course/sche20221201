package com.ribomation.clean_code;

/**
 * Knows how to send an email message
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 */
public interface EmailTransport {
    void send(Message message);
}
