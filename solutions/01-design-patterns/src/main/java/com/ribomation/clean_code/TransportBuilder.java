package com.ribomation.clean_code;

import java.util.Optional;

public class TransportBuilder {
    private Optional<String> smtpServer = Optional.empty();
    private Optional<Integer> smtpPort = Optional.empty();

    public static TransportBuilder create() {
        return new TransportBuilder();
    }

    public TransportBuilder host(String host) {
        smtpServer = Optional.of(host);
        return this;
    }

    public TransportBuilder port(int port) {
        smtpPort = Optional.of(port);
        return this;
    }

    public EmailTransport build() {
        return new DummyEmailTransport(smtpServer.orElse(""), smtpPort.orElse(0));
    }

}
