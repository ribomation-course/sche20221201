package ribomation.clean_code.refactoring;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Accidents {
    public static final String OLD_URL = "http://brottsplatskartan.se/api.php?action=getEvents&period=6000";
    public static final String BASE_URL = "https://raw.githubusercontent.com/devharis/Global-Crime/master/source/swedenXML.xml";

    public static void main(String[] args) throws Exception {
        NodeList ns = getEventData().getElementsByTagName("event");
        for (int i = 0; i < ns.getLength(); i++) {
            printEvent((Element) ns.item(i));
        }
    }

    static Document getEventData() throws SAXException, IOException, ParserConfigurationException {
        Document doc = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(BASE_URL);
        doc.getDocumentElement().normalize();
        return doc;
    }

    static void printEvent(Element e) throws ParseException {
        System.out.printf("*** %s: %s%n\t%s (%.4f,%.4f)%n\t%s%n\t<%s>%n",
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(textOf(e, "date")),
                textOf(e, "title"),
                textOf(e, "place"),
                Double.parseDouble(textOf(e, "lat")),
                Double.parseDouble(textOf(e, "lng")),
                textOf(e, "text").replaceAll("\\s+", " "),
                textOf(e, "link"));
    }

    private static String textOf(Element e, String title) {
        return e.getElementsByTagName(title).item(0).getTextContent();
    }
}
