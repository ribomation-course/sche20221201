package ribomation.clean_code.refactoring;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Accidents2 {
    public static void main(String[] args) throws Exception {
        new Accidents2().run();
    }

    void run() throws Exception {
        var is = inputFactory(true);
        var list = fetch(is);
        print(list);
    }

    InputStream inputFactory(boolean external) throws IOException {
        if (external) {
            String url = "https://raw.githubusercontent.com/devharis/Global-Crime/master/source/swedenXML.xml";
            return new URL(url).openStream();
        } else {
            String classpathResource = "/swedenXML.xml";
            return this.getClass().getResourceAsStream(classpathResource);
        }
    }

    NodeList fetch(InputStream is) throws ParserConfigurationException, IOException, SAXException {
        Document doc = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(is);

        doc.getDocumentElement().normalize();
        return doc.getElementsByTagName("event");
    }

    void print(NodeList events) {
        Supplier<Node> nodeSupplier = new Supplier<Node>() {
            int index = 0;

            @Override
            public Node get() {
                return events.item(index++);
            }
        };

        Stream.generate(nodeSupplier)
                .limit(events.getLength())
                .map(Event::new)
                .forEach(System.out::println);
        ;
    }

    static class Event {
        static class Location {
            final double latitude, longitude;

            public Location(String latitude, String longitude) {
                this(Double.parseDouble(latitude), Double.parseDouble(longitude));
            }

            public Location(double latitude, double longitude) {
                this.latitude = latitude;
                this.longitude = longitude;
            }
        }

        final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String title, place, description, link;
        Date date;
        Location location;

        public Event(Node eventXml) {
            this((Element) eventXml);
        }

        public Event(Element eventXml) {
            title = textOf(eventXml, "title");
            place = textOf(eventXml, "place");
            link = textOfStripEol(eventXml, "link");
            description = textOfStripBlank(eventXml, "text");
            location = new Location(textOf(eventXml, "lat"), textOf(eventXml, "lng"));
            try {
                date = fmt.parse(textOf(eventXml, "date"));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public String toString() {
            return String.format("*** %s: %s%n\t%s (%.4f,%.4f)%n\t%s%n\t<%s>%n",
                    date, title, place, location.latitude, location.longitude, description, link);
        }

        private String textOf(Element e, String tag) {
            NodeList list = e.getElementsByTagName(tag);
            if (list.getLength() == 0) return "";
            return list.item(0).getTextContent();
        }

        private String textOfStripEol(Element e, String tag) {
            return textOf(e, tag).replaceAll("[\r\n\t ]+", "");
        }

        private String textOfStripBlank(Element e, String tag) {
            return textOf(e, tag).replaceAll("\\s+", " ");
        }
    }
}
