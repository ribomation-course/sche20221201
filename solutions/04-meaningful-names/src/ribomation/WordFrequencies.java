package ribomation;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class WordFrequencies {
    public static final String MUSKETEERS_TXT = "./data/the-three-musketeers.txt";
    public static final int MIN_WORD_SIZE = 5;
    public static final int MAX_WORDS = 25;
    public static final String DELIM_PATTERN = "[^a-zA-Z]+";

    public static void main(String[] args) throws Exception {
        WordFrequencies app = new WordFrequencies();
        app.run(openInput(args));
    }

    void run(InputStream is) throws IOException {
        var wordFreqs = load(is, MIN_WORD_SIZE);
        print(wordFreqs, MAX_WORDS);
    }

    static InputStream openInput(String[] args) throws IOException {
        if (args.length == 0) {
            return Files.newInputStream(Path.of(MUSKETEERS_TXT));
        } else {
            return Files.newInputStream(Path.of(args[0]));
        }
    }

    Map<String, Integer> load(InputStream is, int minWordSize) throws IOException {
        var freqs = new HashMap<String, Integer>();
        var input = new BufferedReader(new InputStreamReader(is));
        for (var line = input.readLine(); line != null; line = input.readLine()) {
            for (var word : line.split(DELIM_PATTERN)) {
                if (word.length() < minWordSize) continue;
                word = word.toLowerCase();
                freqs.put(word, freqs.getOrDefault(word, 0) + 1);
            }
        }
        return freqs;
    }

    void print(Map<String, Integer> wordFreqs, int maxWords) {
        List<Map.Entry<String, Integer>> wordFreqPairs = new ArrayList<>(wordFreqs.entrySet());
        wordFreqPairs.sort((lhs, rhs) -> Long.compare(rhs.getValue(), lhs.getValue()));

        int numWords = 0;
        for (var e : wordFreqPairs) {
            System.out.printf("%12s: %d%n", e.getKey(), e.getValue());
            if (++numWords >= maxWords) return;
        }
    }

}
