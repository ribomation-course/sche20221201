package ribomation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WordFrequenciesStreams {
    public static final String MUSKETEERS_TXT = "./data/the-three-musketeers.txt";
    public static final int MIN_WORD_SIZE = 5;
    public static final int MAX_WORDS = 25;
    public static final String DELIM_PATTERN = "[^a-zA-Z]+";

    public static void main(String[] args) throws Exception {
        WordFrequenciesStreams app = new WordFrequenciesStreams();
        app.run(openInput(args));
    }

    void run(InputStream is) throws IOException {
        var wordFrequencies = load(is, MIN_WORD_SIZE);
        print(wordFrequencies, MAX_WORDS);
    }

    static InputStream openInput(String[] args) throws IOException {
        if (args.length == 0) {
            return Files.newInputStream(Path.of(MUSKETEERS_TXT));
        } else {
            return Files.newInputStream(Path.of(args[0]));
        }
    }

    Map<String, Long> load(InputStream is, int minWordSize) throws IOException {
        var input = new BufferedReader(new InputStreamReader(is));
        try (input) {
            return input.lines()
                    .flatMap(Pattern.compile(DELIM_PATTERN)::splitAsStream)
                    .filter(word -> word.length() >= minWordSize)
                    .map(String::toLowerCase)
                    .collect(Collectors.groupingBy(word -> word, Collectors.counting()));
        }
    }

    void print(Map<String, Long> wordFreqs, int maxWords) {
        wordFreqs.entrySet().stream()
                .sorted((lhs, rhs) -> Long.compare(rhs.getValue(), lhs.getValue()))
                .limit(maxWords)
                .forEach(e -> System.out.printf("%12s: %d%n", e.getKey(), e.getValue()));
    }

}
