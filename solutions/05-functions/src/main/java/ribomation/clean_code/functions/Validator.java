package ribomation.clean_code.functions;

import static java.lang.Integer.parseInt;
import static java.time.LocalDate.now;
import static java.time.format.DateTimeFormatter.BASIC_ISO_DATE;

public class Validator {
    String normalize(String pnr) {
        if (pnr.length() == 12) return pnr;
        if (pnr.length() == 13) return pnr.replaceFirst("[+-]", "");
        if (pnr.contains("+")) return "19" + pnr.replaceFirst("[+]", "");

        pnr = pnr.replaceFirst("-", "");
        int birthYear   = 2000 + parseInt(pnr.substring(0, 2));
        int currentYear = now().getYear();
        if (currentYear < birthYear) return "19" + pnr;

        String yearPrefix = ((currentYear != birthYear) || isYoung(pnr) ? "20" : "19");
        return yearPrefix + pnr;
    }

    private boolean isYoung(String pnr) {
        String todayStr   = now().format(BASIC_ISO_DATE);
        String birthStr   = "20" + pnr.substring(0, 6);
        return birthStr.compareTo(todayStr) > 0;
    }
}
