package ribomation.clean_code.classes;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Encapsulates app and build info
 */
public class MetaData {
    public String getAppName() {
        return "AccountsSwingApp";
    }

    public int getVersion() {
        return 42;
    }

    public String getCompany() {
        return "Foobar Ltd";
    }

    public String getDate() {
        return new SimpleDateFormat("d MMM yyyy").format(new Date());
    }

    public int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }
}
