package ribomation.clean_code.classes.domain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AccountRepo {
    private final String RESOURCE_PATH = "/accounts.csv";
    private List<Account> accounts = new ArrayList<>();

    public AccountRepo() {
        load(RESOURCE_PATH);
    }

    public String getFilename() {
        return RESOURCE_PATH;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public int getRowCount() {
        return accounts.size();
    }
    public int getColumnCount() {
        return accounts.get(0).getCount();
    }
    public String getColumnName(int column) {
        return accounts.get(0).getName(column);
    }

    public void load(String resourcePath) {
        var is = getClass().getResourceAsStream(resourcePath);
        var in = new BufferedReader(new InputStreamReader(is));
        try (in) {
            accounts = in
                    .lines()
                    .skip(1)
                    .map(csv -> fromCSV(csv, ","))
                    .collect(Collectors.toList())
            ;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Account fromCSV(String csv, String delim) {
        var f = csv.split(delim);
        var ix = 0;
        var iban = f[ix++];
        var amount = f[ix++];
        var currency = f[ix++];
        var owner = f[ix++];
        var profession = f[ix++];
        var country = f[ix++];
        return new Account(iban, Double.parseDouble(amount), currency, owner, profession, country);
    }


}
