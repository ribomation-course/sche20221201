package ribomation.clean_code.classes.domain;

import javax.swing.table.AbstractTableModel;

public class AccountTableModel extends AbstractTableModel {
    private final AccountRepo repo;

    public AccountTableModel(AccountRepo repo) {
        this.repo = repo;
    }

    @Override
    public int getRowCount() {
        return repo.getRowCount();
    }

    @Override
    public int getColumnCount() {
        return repo.getColumnCount();
    }

    @Override
    public Object getValueAt(int row, int column) {
        return repo.getAccounts().get(row).getValue(column);
    }

    @Override
    public String getColumnName(int column) {
        return repo.getColumnName(column);
    }

    @Override
    public Class<?> getColumnClass(int column) {
        return String.class;
    }

    public double getAmountSum() {
        return repo.getAccounts().stream()
                .mapToDouble(Account::getAmount)
                .sum();
    }

    public long getCurrencyCount() {
        return repo.getAccounts().stream()
                .map(Account::getCurrency)
                .distinct()
                .count();
    }

    public long getCountryCount() {
        return repo.getAccounts().stream()
                .map(Account::getCountry)
                .distinct()
                .count();
    }

}


