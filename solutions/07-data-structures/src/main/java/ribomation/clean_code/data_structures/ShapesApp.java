package ribomation.clean_code.data_structures;

import ribomation.clean_code.data_structures.shape.Shape;
import ribomation.clean_code.data_structures.shape.ShapeFactory;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ShapesApp {
    public static void main(String[] args) throws Exception {
        new ShapesApp().run(args);
    }

    void run(String[] args) throws IOException {
        var shapes = load(args);
        print(shapes, 12);
        area(shapes);
        perimeter(shapes);
    }

    List<Shape> load(String[] args)  {
        var filename = (args.length > 0) ? args[0] : "./src/main/resources/shapes.csv";
        return new ShapeFactory(filename).load();
    }

    void print(List<Shape> shapes, int maxShapesCount) {
        shapes.stream()
                .limit(maxShapesCount)
                .map(Shape::toString)
                .forEach(System.out::println);
    }

    void area(List<Shape> shapes) {
        var area = shapes.stream()
                .mapToDouble(Shape::area)
                .sum();
        System.out.printf(Locale.ENGLISH, "Total area = %.3f%n", area);
    }

    void perimeter(List<Shape> shapes) {
        var perimeter = shapes.stream()
                .mapToDouble(Shape::perimeter)
                .sum();
        System.out.printf(Locale.ENGLISH, "Total perimeter = %.3f%n", perimeter);
    }

}
