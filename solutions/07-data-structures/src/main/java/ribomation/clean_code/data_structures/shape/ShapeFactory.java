package ribomation.clean_code.data_structures.shape;

import ribomation.clean_code.data_structures.variants.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class ShapeFactory {
    private final Path inputPath;

    public ShapeFactory(String filename)  {
        this.inputPath = Path.of(filename);
        if (!Files.isReadable(inputPath)) {
            throw new RuntimeException(filename);
        }
    }

    public List<Shape> load()  {
        try {
            var stream = Files.lines(inputPath);
            try (stream) {
                return stream
                        .skip(1)
                        .map(this::fromCSV)
                        .collect(Collectors.toList());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Shape fromCSV(String csv) {
        String[] fields = csv.split(",");
        String type = fields[0];
        int x = Integer.parseInt(fields[1]);
        int y = Integer.parseInt(fields[2]);
        int w = Integer.parseInt(fields[3]);
        int h = Integer.parseInt(fields[4]);

        Point loc = new Point(x, y);
        switch (type) {
            case "rect":
                return new Rectangle(loc, w, h);
            case "squa":
                return new Square(loc, w);
            case "tria":
                return new Triangle(loc, w, h);
            case "circ":
                return new Circle(loc, w);
        }

        throw new IllegalArgumentException("no such type: " + type);
    }

}
