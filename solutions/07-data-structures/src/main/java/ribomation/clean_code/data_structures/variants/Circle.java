package ribomation.clean_code.data_structures.variants;

import ribomation.clean_code.data_structures.shape.Point;
import ribomation.clean_code.data_structures.shape.Shape;

public class Circle extends Shape {
    private int radius;

    public Circle(Point position, int radius) {
        super(position);
        this.radius = radius;
    }

    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public double perimeter() {
        return Math.PI * 2 * radius;
    }

    @Override
    protected String dimension() {
        return String.format("%d", radius);
    }
}
