package ribomation.clean_code.data_structures.variants;

import ribomation.clean_code.data_structures.shape.Point;
import ribomation.clean_code.data_structures.shape.Shape;

public class Rectangle extends Shape {
    protected int width;
    protected int height;

    public Rectangle(Point position, int width, int height) {
        super(position);
        this.width = width;
        this.height = height;
    }

    @Override
    public double area() {
        return width * height;
    }

    @Override
    public double perimeter() {
        return 2 * (width + height);
    }

    @Override
    protected String dimension() {
        return String.format("%d,%d", width, height);
    }
}
