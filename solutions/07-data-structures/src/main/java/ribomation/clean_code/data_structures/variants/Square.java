package ribomation.clean_code.data_structures.variants;

import ribomation.clean_code.data_structures.shape.Point;

public class Square extends Rectangle {
    public Square(Point position, int side) {
        super(position, side, side);
    }

    @Override
    protected String dimension() {
        return String.format("%d", width);
    }
}
