package ribomation.clean_code.data_structures.variants;

import ribomation.clean_code.data_structures.shape.Point;
import ribomation.clean_code.data_structures.shape.Shape;

public class Triangle extends Shape {
    protected int base;
    protected int height;

    public Triangle(Point position, int base, int height) {
        super(position);
        this.base = base;
        this.height = height;
    }

    @Override
    public double area() {
        return base * height / 2D;
    }

    @Override
    public double perimeter() {
        return base + 2 * Math.sqrt(base * base / 4D + height * height);
    }

    @Override
    protected String dimension() {
        return String.format("%d,%d", base, height);
    }
}
