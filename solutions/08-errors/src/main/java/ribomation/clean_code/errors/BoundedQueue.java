package ribomation.clean_code.errors;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;

public class BoundedQueue<T> {
    private final CircularBuffer<T> queue;
    private final AtomicBoolean     done = new AtomicBoolean(false);

    public BoundedQueue(int capacity) {
        queue = new CircularBuffer<>(capacity);
    }

    public synchronized void put(T x) {
        try {
            waitUntil(CircularBuffer::full);
            queue.put(x);
        } finally {
            notifyAll();
        }
    }

    public synchronized T get() {
        try {
            waitUntil(CircularBuffer::empty);
            return queue.get();
        } finally {
            notifyAll();
        }
    }

    private void waitUntil(Predicate<CircularBuffer<T>> until) {
        while (until.test(queue)) {
            try {
                wait();
            } catch (InterruptedException ignore) {}
        }
    }

    public void setDone() {
        done.set(true);
    }

    public boolean isDone() {
        return queue.empty() && done.get();
    }
}
