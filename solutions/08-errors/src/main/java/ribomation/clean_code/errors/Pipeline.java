package ribomation.clean_code.errors;

import java.util.stream.IntStream;

public class Pipeline {
    public static void main(String[] args) {
        int N = args.length == 0 ? 512 : Integer.parseInt(args[0]);
        Pipeline app = new Pipeline();
        app.run(N);
    }

    void run(int N) {
        BoundedQueue<Integer> q        = new BoundedQueue<>(2);
        Consumer<Integer>     consumer = new Consumer<>(q);
        consumer.start();

        IntStream.rangeClosed(1, N).forEach(q::put);
        q.setDone();

        try {
            consumer.join();
        } catch (InterruptedException ignore) {}
    }
}
