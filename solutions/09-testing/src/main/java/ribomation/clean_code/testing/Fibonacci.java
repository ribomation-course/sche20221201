package ribomation.clean_code.testing;

import java.math.BigInteger;

/**
 * Computes FIN(n)
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2016-04-28
 */
public class Fibonacci extends CachingUnaryFunction {

    /**
     * Uses the formula: FIB(n) = fib(n-2) + fib(n-1), n > 2
     *
     * @param n
     * @return fib(n-2) + fib(n-1)
     */
    @Override
    protected BigInteger computeResult(int n) {
        if (n <= 2) {
            return BigInteger.ONE;
        }

        return compute(n - 2).add(compute(n - 1));
    }
}
