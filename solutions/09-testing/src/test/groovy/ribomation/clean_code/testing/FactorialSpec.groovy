package ribomation.clean_code.testing

import spock.lang.Specification

class FactorialSpec extends Specification {

    def 'simple values should pass'() {
        given: 'a Factorial object'
        def fac = new Factorial()

        expect:
        fac.compute(arg) == result

        where:
        arg || result
        4   || 2 * 3 * 4
        5   || 2 * 3 * 4 * 5
        6   || 2 * 3 * 4 * 5 * 6
    }

    def 'fac(100) should be insanely large'() {
        given: 'a Factorial object'
        def fac = new Factorial()

        when:
        def result = fac.compute(100)

        then:
        result == 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
    }

}
