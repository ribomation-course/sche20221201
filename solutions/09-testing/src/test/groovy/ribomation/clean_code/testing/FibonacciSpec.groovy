package ribomation.clean_code.testing

import spock.lang.Specification

class FibonacciSpec extends Specification {

    def 'simple values should pass'() {
        given: 'a Fibonacci object'
        def fib = new Fibonacci()

        expect:
        fib.compute(arg) == result

        where:
        arg || result
        2   || 1
        5   || 5
        10  || 55
    }

    def 'fib(42) should be 267_914_296'() {
        given: 'a Fibonacci object'
        def fib = new Fibonacci()

        when:
        def result = fib.compute(42)

        then:
        result == 267_914_296
    }

    def 'fib(100) should be something large'() {
        given: 'a Fibonacci object'
        def fib = new Fibonacci()

        when:
        def result = fib.compute(100)

        then:
        result == 354224848179261915075
        result.class == BigInteger
    }

}
