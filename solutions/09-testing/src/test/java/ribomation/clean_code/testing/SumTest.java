package ribomation.clean_code.testing;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class SumTest {
    private Sum target;

    @Before
    public void setUp() throws Exception {
        target = new Sum();
    }

    @Test
    public void simple_values_should_pass() throws Exception {
        assertThat(target.compute(2), is(BigInteger.valueOf(3)));
        assertThat(target.compute(10), is(BigInteger.valueOf(55)));
        assertThat(target.compute(100), is(BigInteger.valueOf(5050)));
    }

    @Test
    public void arg_10000_should_pass() throws Exception {
        assertThat(target.compute(10_000), is(BigInteger.valueOf(50_005_000)));
    }

    @Test
    public void arg_1_000_000_should_pass() throws Exception {
        assertThat(target.compute(1_000_000), is(new BigInteger("500000500000")));
    }
}