package ribomation.clean_code.classes;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.*;
import java.awt.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class Accounts extends JFrame {
    public static void main(String[] args) throws Exception {
        Accounts app = new Accounts();
        app.parseArgs(args);
        TableModel model = app.loadModel();
        app.initGui(model);
        app.showGui();
    }

    void parseArgs(String[] args) {
        if (args.length > 0) {
            filename = args[0];
        }
    }

    String         filename = "./src/main/resources/accounts.csv";
    List<String[]> data     = new ArrayList<>();
    JTable                     tbl;
    TableModel                 model;
    TableRowSorter<TableModel> sorter;
    JTextField                 filter;

    List<String[]> load(String filename) throws Exception {
        if (!Paths.get(filename).toFile().canRead()) {
            throw new IllegalArgumentException("Cannot find file: " + filename);
        }

        return Files.lines(Paths.get(filename))
                .map((line) -> line.split((",")))
                .collect(Collectors.toList())
                ;
    }


    TableModel loadModel() throws Exception {
        this.data = load(getFilename());

        this.model = new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return data.size() - 1;
            }

            @Override
            public int getColumnCount() {
                return data.get(0).length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return data.get(rowIndex + 1)[columnIndex];
            }

            @Override
            public String getColumnName(int column) {
                return data.get(0)[column].toUpperCase();
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return String.class;
            }
        };

        return this.model;
    }

    void initGui(TableModel model) {
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle(String.format("Accounts :: Version %d (%s) :: (c) %s, %d.",
                getVersion(), getDate(), getCompany(), getYear()));

        tbl = new JTable(model);
        tbl.setAutoCreateRowSorter(true);
        tbl.setFillsViewportHeight(true);
        tbl.setPreferredScrollableViewportSize(new Dimension(800, 500));

        final int N = model.getColumnCount();
        for (int k = 0; k < N; ++k) {
            Component   c   = tbl.getDefaultRenderer(model.getColumnClass(k)).getTableCellRendererComponent(tbl, data.get(1)[k], false, false, 1, k);
            TableColumn col = tbl.getColumnModel().getColumn(k);
            col.setMinWidth(c.getPreferredSize().width);

            if (model.getColumnName(k).equalsIgnoreCase("amount")) {
                DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
                renderer.setHorizontalAlignment(JLabel.RIGHT);
                col.setCellRenderer(renderer);
            }

            if (model.getColumnName(k).equalsIgnoreCase("currency")) {
                DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
                renderer.setHorizontalAlignment(JLabel.CENTER);
                col.setCellRenderer(renderer);
            }
        }

        sorter = new TableRowSorter<>(model);
        tbl.setRowSorter(sorter);

        filter = new JTextField();
        filter.getDocument().addDocumentListener(
                new DocumentListener() {
                    public void changedUpdate(DocumentEvent e) {
                        newFilter();
                    }

                    public void insertUpdate(DocumentEvent e) {
                        newFilter();
                    }

                    public void removeUpdate(DocumentEvent e) {
                        newFilter();
                    }
                });

        JPanel form = new JPanel(new SpringLayout());
        JLabel lbl  = new JLabel("Filter column values:", SwingConstants.TRAILING);
        lbl.setLabelFor(filter);
        form.add(lbl);
        form.add(filter);
        SpringUtilities.makeCompactGrid(form, 1, 2, 6, 6, 6, 6);

        int    infoNumRows = 4;
        JPanel info        = new JPanel(new SpringLayout());
        {
            JLabel l = new JLabel("Account File:", JLabel.TRAILING);
            info.add(l);
            JTextField f = new JTextField(16);
            f.setEditable(false);
            l.setLabelFor(f);
            info.add(f);
            f.setText(getFilename());
        }
        {
            JLabel l = new JLabel("SUM Amount:", JLabel.TRAILING);
            info.add(l);
            JTextField f = new JTextField(16);
            f.setEditable(false);
            l.setLabelFor(f);
            info.add(f);
            f.setText(String.format(Locale.ENGLISH, "%.2f", getAmountSum()));
        }
        {
            JLabel l = new JLabel("# Currencies:", JLabel.TRAILING);
            info.add(l);
            JTextField f = new JTextField(16);
            f.setEditable(false);
            l.setLabelFor(f);
            info.add(f);
            f.setText(String.format(Locale.ENGLISH, "%d", getCurrencyCount()));
        }
        {
            JLabel l = new JLabel("# Countries:", JLabel.TRAILING);
            info.add(l);
            JTextField f = new JTextField(16);
            f.setEditable(false);
            l.setLabelFor(f);
            info.add(f);
            f.setText(String.format(Locale.ENGLISH, "%d", getCountryCount()));
        }
        SpringUtilities.makeCompactGrid(info,
                infoNumRows, 2, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        JScrollPane scrollPane = new JScrollPane(tbl);
        this.getContentPane().add(info, BorderLayout.NORTH);
        this.getContentPane().add(scrollPane, BorderLayout.CENTER);
        this.getContentPane().add(form, BorderLayout.SOUTH);
    }

    void newFilter() {
        try {
            RowFilter<TableModel, Object> rf = RowFilter.regexFilter(filter.getText(), 0, 1, 2, 3, 4, 5);
            sorter.setRowFilter(rf);
        } catch (Exception e) {
            System.out.println("e = " + e);
            return;
        }
    }

    void showGui() {
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    int getVersion() {
        return 42;
    }

    String getDate() {
        return new SimpleDateFormat("d MMM yyyy").format(new Date());
    }

    String getCompany() {
        return "Foobar Ltd";
    }

    int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    String getFilename() {
        return filename;
    }

    double getAmountSum() {
        return data.stream()
                .skip(1)
                .map((d) -> d[1])
                .mapToDouble(Double::parseDouble)
                .sum()
        ;
    }

    long getCurrencyCount() {
        return data.stream()
                .skip(1)
                .map((d) -> d[1])
                .distinct()
                .count()
                ;
    }

    long getCountryCount() {
        return data.stream()
                .skip(1)
                .map((d) -> d[5])
                .distinct()
                .count()
                ;
    }
}
