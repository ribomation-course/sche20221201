package ribomation.clean_code.errors;

public class CircularBuffer<T> {
    private T[] buffer;
    private int putIdx = 0, getIdx = 0, size = 0;

    @SuppressWarnings("unchecked")
    public CircularBuffer(int capacity) {
        buffer = (T[]) new Object[capacity];
    }

    public boolean full() {
        return size >= buffer.length;
    }

    public boolean empty() {
        return size == 0;
    }

    public int size() {
        return this.size;
    }

    public int capacity() {
        return buffer.length;
    }

    public int put(T x) {
        if (x == null) return -2;
        if (full()) return -1;

        buffer[putIdx++] = x;
        putIdx %= buffer.length;
        ++size;

        return 0;
    }

    public int get(T[] x) {
        if (x == null) return -3;
        if (x.length != 1) return -2;
        if (empty()) return -1;

        x[0] = buffer[getIdx++];
        getIdx %= buffer.length;
        --size;

        return 0;
    }

}
