package ribomation.clean_code.refactoring;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.text.SimpleDateFormat;

public class Accidents {
    public static void main(String[] args) throws Exception {
        Document doc = DocumentBuilderFactory
        				.newInstance()
        				.newDocumentBuilder()
        				.parse(this.getClass().getResourceAsStream("/swedenXML.xml"));
        doc.getDocumentElement().normalize();
        NodeList ns = doc.getElementsByTagName("event");
        for (int i = 0; i < ns.getLength(); i++) {
            Element e = (Element) ns.item(i);
            System.out.printf("*** %s: %s%n\t%s (%.4f,%.4f)%n\t%s%n\t<%s>%n", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(e.getElementsByTagName("date").item(0).getTextContent()), e.getElementsByTagName("title").item(0).getTextContent(), e.getElementsByTagName("place").item(0).getTextContent(), Double.parseDouble(e.getElementsByTagName("lat").item(0).getTextContent()), Double.parseDouble(e.getElementsByTagName("lng").item(0).getTextContent()), e.getElementsByTagName("text").item(0).getTextContent().replaceAll("\\s+", " "), e.getElementsByTagName("link").item(0).getTextContent());
        }
    }
}
