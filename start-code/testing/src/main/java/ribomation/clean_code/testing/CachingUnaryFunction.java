package ribomation.clean_code.testing;

import java.math.BigInteger;

/**
 * Prevents computing the same value as previously.
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2016-04-28
 */
public abstract class CachingUnaryFunction implements UnaryFunction {
    private ArgumentAndResultCache cache = new ArgumentAndResultCache();

    @Override
    public final BigInteger compute(int arg) {
        if (arg < 0) {
            throw new IllegalArgumentException("negative argument: " + arg);
        }

        if (arg == 0) {
            return BigInteger.ZERO;
        }

        if (arg == 1) {
            return BigInteger.ONE;
        }

        if (cache.has(arg)) {
            return cache.get(arg);
        }

        BigInteger result = computeResult(arg);
        cache.put(arg, result);
        return result;
    }

    protected abstract BigInteger computeResult(int arg);

    public void clear() {
        cache.clear();
    }
}

