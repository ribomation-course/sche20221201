package ribomation.clean_code.testing;

import java.math.BigInteger;

/**
 * Computes n!
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2016-04-28
 */
public class Factorial extends CachingUnaryFunction {

    /**
     * Uses the formula: PROD(1..n) = n * prod(n-1), n > 1
     *
     * @param n
     * @return n * prod(n-1)
     */
    @Override
    protected BigInteger computeResult(int n) {
        return BigInteger.valueOf(n).multiply(compute(n - 1));
    }
}
