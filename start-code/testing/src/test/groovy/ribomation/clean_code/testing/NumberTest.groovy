package ribomation.clean_code.testing

import spock.lang.Specification

class NumberTest extends Specification {

    def 'simple args to sum should pass'() {
        given: 'a Sum object'
        def sum = new Sum()

        expect:
        sum.compute(arg) == result as BigInteger

        where:
        arg || result
        1   || 1
        5   || (1+2+3+4+5)
        10  || 55
    }


}
